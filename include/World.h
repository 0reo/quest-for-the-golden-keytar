#ifndef World_H
#define World_H

#include"Character.h"

using namespace std;

class World:public CollisionObject
{
    public:
        World();
        World(char* filename, char* charName, int frames, SDL_Rect frame);
        virtual ~World();

        void apply_surface( int x, int y, SDL_Surface* source, SDL_Surface* destination, SDL_Rect* clip );
        void addChar(Character* charr);                     ///add a character to the game world
        void removeChar(string charr);                       ///remove a character to the game world
        char checkForCollisions();                          ///looks for all collision
        void setCollisionAreas();                           ///add ALL collision areas
        void addCollisionArea(CollisionObject* area);       ///add one collision area
        void removeCollisionArea(string area);              ///remove one collision area
        void update(int timer, SDL_Surface* screen);        ///updates time step and calls movement, drawWorld
        void movement(int delta);                           ///applies all movement to characters
        void drawWorld(SDL_Surface* screen);                ///draws everything to the screen



    protected:
    private:
    SDL_Rect worldFrame;       ///the display window for the world.  shows one frame of the world sheet
    SDL_Rect worldLocation;    ///the location on the screen where the world is


    map<int, bool> states;
    map<int, bool>::iterator statesItr;
    map<string, Character*> characters;
    map<string, Character*>::iterator charactersItr;
    map<string, CollisionObject*> collisionAreas;
    map<string, CollisionObject*>::iterator collisionItr;

friend class CollisionObject;
};

#endif // World_H
