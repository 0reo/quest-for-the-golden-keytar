#ifndef MONSTER_H
#define MONSTER_H

#include "Character.h"


class monster : public Character
{
    public:
        monster();
        monster(char* filename, char* charName, int frames, SDL_Rect frame);
        virtual ~monster();
    protected:
    private:
};

#endif // MONSTER_H
