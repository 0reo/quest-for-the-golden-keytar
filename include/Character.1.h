#ifndef CHARACTER_H
#define CHARACTER_H
#include "CollisionObject.h"

/***char states***/
#define STATE int
#define RUN  0
#define JUMP  8
#define HIT  20

#define CROUCH	 14

#define TAKEHIT  25

#define IDLE  18
#define DIE  64
#define TRANSFORM  128
#define STUN  256
#define FORWARD 1
#define BACKWARDS -1
#define COLLIDE 0
#define STOMP 0


using namespace std;
typedef struct Vector2D {
    float x, y;
};

class Character:public CollisionObject
{
    public:
        Character();
        Character(char* filename, char* charName, int frames, SDL_Rect frame);
        //virtual ~Character();
        Vector2D getSize();
        void setDirection(int i);
        void setState(int state, bool off = false);
        void run(float vel);
        void jump(float jumpForce, int step);
        char* getName();
        void move(int delta);
        void addsheet(char* filename, int frames );
        void hold(int frames, bool state);
        void idle();

        friend class World;

    protected:
    private:

    int direction;
    int directiony;
    Vector2D velocity;
    Vector2D maxVel;
    Vector2D accel;


    float jforce;
    Vector2D frameSize;          ///size of animation frames
    bool onGround;

    map<int, bool> states;
    map<int, bool>::iterator statesItr;

    SDL_Surface* sheet2; ///secondary sprite sheet
    SDL_Surface* sheet3; ///secondary sprite sheet
    SDL_Surface* sheet4; ///secondary sprite sheet


    /***STATUS STUFF**/
    int health;
    int strength;



};

#endif // CHARACTER_H
