#ifndef COLLISIONOBJECT_H
#define COLLISIONOBJECT_H

#ifdef __cplusplus
    #include <cstdlib>
#else
    #include <stdlib.h>
#endif
#ifdef __APPLE__
#include <SDL/SDL.h>
#else
#include <SDL/SDL.h>
#endif
#include <iostream>
#include"SDL/SDL_image.h"



#include <map>
#include <vector>

using namespace std;
typedef struct Vector2D {
    float x, y;
};

class CollisionObject
{
    public:
        CollisionObject();
        CollisionObject(char* filename, char* charName);
        virtual ~CollisionObject();
        SDL_Surface* getImage();                                    ///returns world sheet
        SDL_Rect* getFrame();
        void setPosition(SDL_Rect position);                        ///sets character position
        SDL_Rect* getPosition();                                    ///returns character position
        void move(int x, int y);                                    ///moves character by ammount sent
        void checkForCollisions();                                  ///check if object has collided with something
        void setCollidable();                                       ///set/update collision bounds
        bool collisionVert(float side);                             ///check for a collision on vertical axis
        bool collisionHorz(float side);                             ///check for collision on horizontal axis
        void applyForce(Vector2D frc);                              ///apply force for movement
        Vector2D getForce();                                        ///get current force applied
        Uint32 get_pixel32( SDL_Surface *surface, int x, int y );
        void put_pixel32( SDL_Surface *surface, int x, int y, Uint32 pixel );
        SDL_Surface* flip_surface( SDL_Surface *surface, int flags );

    float top;
    float bottom;
    float left;
    float right;

    /**status of potential collision on a side of collision object*/
    bool topCol;
    bool botCol;
    bool leftCol;
    bool rightCol;
    /******************************************************/
    Vector2D normal;
    Vector2D force;         ///force vector, for thing such as jumping
    //Flip flags
     int FLIP_VERTICAL;
     int FLIP_HORIZONTAL;


    protected:
    char* name;                 ///name to identify character with
    SDL_Surface* tmp;           ///temp space for loaded sprite sheet
    SDL_Rect spriteFrame;       ///display window for the sprite.  shows one frame of the sprite sheet
    SDL_Rect spriteLocation;    ///the location on the screen where the sprite is

        float gravity;
    float mass;

    vector<SDL_Surface*> sheets;
    vector<SDL_Surface*>::iterator sheetsItr;
        int currentSheet;


    private:

};

#endif // COLLISIONOBJECT_H
