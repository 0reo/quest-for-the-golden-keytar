#ifndef STATEMACHINE_H
#define STATEMACHINE_H

#include <vector>
using namespace std;

class State;

class Statemachine
{
    public:
        Statemachine();
        virtual ~Statemachine();
        void Init();
        void Cleanup();

        void ChangeState(State* state);
        void PushState(State* state);
        void PopState();

        void HandleEvents();
        void Update();
        void Draw();

        bool Running() { return m_running; }
        void Quit() { m_running = false; }

    protected:
        vector<State*> states;

          bool m_running;
    private:
};

#endif // STATEMACHINE_H
