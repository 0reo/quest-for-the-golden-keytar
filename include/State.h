#ifndef STATE_H
#define STATE_H

#include "Statemachine.h"
class State
{
    public:
        /** Default constructor */
        State();
        /** Default destructor */
        virtual ~State();


        virtual void Init() = 0;
        virtual void Cleanup() = 0;

        virtual void Pause() = 0;
        virtual void Resume() = 0;

        virtual void HandleEvents(Statemachine* machine) = 0;
        virtual void Update(Statemachine* machine) = 0;
        virtual void Draw(Statemachine* machine) = 0;

        void ChangeState(Statemachine* machine, State* state)
        {
            machine->ChangeState(state);
        }
    protected:
    private:
        unsigned int m_Counter; //!< Member variable "m_Counter"
};

#endif // STATE_H
