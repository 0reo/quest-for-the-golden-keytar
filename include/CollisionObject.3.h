#ifndef COLLISIONOBJECT_H
#define COLLISIONOBJECT_H

#ifdef __cplusplus
    #include <cstdlib>
#else
    #include <stdlib.h>
#endif
#ifdef __APPLE__
#include <SDL/SDL.h>
#else
#include <SDL/SDL.h>
#endif
#include <iostream>
#include"SDL/SDL_image.h"



#include <map>
#include <vector>

using namespace std;
typedef struct Vector2D {
    float x, y;
};

class CollisionObject
{
    public:
        CollisionObject();
        CollisionObject(char* filename, char* charName);
        virtual ~CollisionObject();
        SDL_Surface* getImage();     ///returns world sheet
        SDL_Rect* getFrame();
        void setPosition(SDL_Rect position);
        SDL_Rect* getPosition();
        void checkForCollisions();
        void setCollidable();
        bool collisionVert(float side);
        bool collisionHorz(float side);
        void applyForce(Vector2D frc);
        Vector2D getForce();
        Uint32 get_pixel32( SDL_Surface *surface, int x, int y );
        void put_pixel32( SDL_Surface *surface, int x, int y, Uint32 pixel );
        SDL_Surface* flip_surface( SDL_Surface *surface, int flags );

    float top;
    float bottom;
    float left;
    float right;

    /**status of potential collision on a side of collision object*/
    bool topCol;
    bool botCol;
    bool leftCol;
    bool rightCol;
    /******************************************************/
    Vector2D normal;
    Vector2D force;         ///force vector, for thing such as jumping
    //Flip flags
     int FLIP_VERTICAL;
     int FLIP_HORIZONTAL;


    protected:
    char* name;             ///name to identify character with
    SDL_Surface* tmp;       ///temp space for loaded sprite sheet
    SDL_Rect spriteFrame;   ///display window for the sprite.  shows one frame of the sprite sheet
    SDL_Rect spriteLocation;    ///the location on the screen where the sprite is

        float gravity;
    float mass;

    vector<SDL_Surface*> sheets;
    vector<SDL_Surface*>::iterator sheetsItr;
        int currentSheet;


    private:

};

#endif // COLLISIONOBJECT_H
