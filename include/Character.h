#ifndef CHARACTER_H
#define CHARACTER_H
#include "CollisionObject.h"

/***char states***/
#define STATE int
#define RUN  0
#define JUMP  8
#define HIT  20

#define CROUCH	 14

#define TAKEHIT  25

#define IDLE  18
#define DIE  64
#define TRANSFORM  128
#define STUN  256
#define FORWARD 1
#define BACKWARDS -1
#define COLLIDE 0
#define STOMP 0


typedef struct Action {
   char* name;
   int frames;
   int position;
};

class Character:public CollisionObject
{
    public:
        Character();
        Character(char* filename, char* charName, int frames, SDL_Rect frame);
        //virtual ~Character();
        Vector2D getSize();                                     ///get size of sprite
        void setDirection(int i);                               ///set direction sprite is facing
        int getDirection();                                     ///get direction sprite is facing
        void setState(int state, bool off = false);             ///set the characters state
        void run(float vel);                                    ///run state method
        void jump(float jumpForce, int step);                   ///jump state method
        void hit();                                             ///hit state method
        bool getSwinging();                                      ///check if you're swinging the keytar
        //void movement(int delta);
        void addsheet(char* filename, int frames );             ///add sprite sheet
        void hold(int frames, bool state);
        void idle();                                            ///idle state method
        void addAction(char* name, int frames, int position);   ///add action/state

        friend class World;

    protected:
    private:

    int direction;
    int directiony;
    int swingCount;
    Vector2D velocity;
    Vector2D maxVel;
    Vector2D accel;
    vector<Action> actions;


    float jforce;
    Vector2D frameSize;          ///size of animation frames
    bool onGround;

    map<int, bool> states;
    map<int, bool>::iterator statesItr;

    SDL_Surface* sheet2; ///secondary sprite sheet
    SDL_Surface* sheet3; ///secondary sprite sheet
    SDL_Surface* sheet4; ///secondary sprite sheet


    /***STATUS STUFF**/
    int health;
    int strength;



};

#endif // CHARACTER_H
