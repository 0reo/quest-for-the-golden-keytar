#ifdef __cplusplus
#include <cstdlib>
#else
#include <stdlib.h>
#endif
#ifdef __APPLE__
#include <SDL/SDL.h>
#else
#include <SDL/SDL.h>
#endif
#include"SDL/SDL_image.h"
#include "SDL/SDL_mixer.h"
#include "include/CollisionObject.h"
#include "include/Character.h"
#include "include/World.h"
#include "include/monster.h"
#include <iostream>
using namespace std;
int timer, delta;
int main ( int argc, char** argv )
{
    //freopen( "CON", "w", stdout );
    //freopen( "CON", "w", stderr );

/**MUSIC****************************************/
//The music that will be played
Mix_Music *music = NULL;

//The sound effects that will be used
//Initialize SDL_mixer
    if( Mix_OpenAudio( 22050, MIX_DEFAULT_FORMAT, 2, 4096 ) == -1 )
    {
        return false;
    }

/**MUSIC END*************************************/


    /// initialize SDL video
    if ( SDL_Init( SDL_INIT_EVERYTHING ) < 0 )
    {
        printf( "Unable to init SDL: %s\n", SDL_GetError() );
        return 1;
    }

    /// make sure SDL cleans up before exit
    atexit(SDL_Quit);


    SDL_WM_SetCaption( "Quest for the Golden Keytar", NULL );

    // create a new window
    SDL_Surface* screen = SDL_SetVideoMode(1024, 768, 32,
                                           SDL_HWSURFACE|SDL_DOUBLEBUF);
    if ( !screen )
    {
        printf("Unable to set 1024x768 video: %s\n", SDL_GetError());
        return 1;
    }


/**SET UP GAME WORLD***************************************************************************************/
    SDL_Rect dstrect;
    SDL_Rect clip;  ///this will hold what part of the image will will be loading

    ///Clip range for the top left
    clip.x = 0;
    clip.y = 0;
    clip.w = 4186;
    clip.h = 1024;


    World* level = new World("../images/Map.png", "world", 1, clip);
    dstrect.x = (screen->w - clip.w );  ///position
    dstrect.y = (1000);                 ///position
    level->setPosition(dstrect);
    level->setCollidable();             ///everything in the game world that requires collisions needs to have this set


    /// loading lomy sprite
    clip.x = 0;
    clip.y = 0;
    clip.w = 190;
    clip.h = 239;
    Character* lomy = new Character("../images/lomy_clean.png", "lomy", 32, clip);


    /// loading goat monster sprite
    clip.x = 0;
    clip.y = 0;
    clip.w = 213;
    clip.h = 239;
    monster* goat = new monster("../images/goat.png", "goat", 0, clip);/*************************/



    /**add's all sprites sheets for lomy************************/
    lomy->addsheet("../images/KeytarFlipped.png", 32);
    lomy->addsheet("../images/Alone-Sprites.png", 27);
    lomy->addsheet("../images/Alone-Sprites-Flip.png", 27);
    lomy->addsheet("../images/Gold-Sprites.png", 25);
    lomy->addsheet("../images/Gold-Sprites-Flip.png", 25);
    /**add's all sprites sheets for lomy END********************/
    /**add's all action data for lomy******/
    lomy->addAction("Run", 8, 0);
    /**add's all action data for lomy END**/

    /**add's all sprites sheets for goat monster****************/
    goat->addsheet("../images/goat-flip.png", 32);
    /**add's all sprites sheets for goat monster END************/
    /**add's all action data for goat monster************/
    goat->addAction("Run", 6, 0);
    /**add's all action data for goat monster END********/



    /**add all characters into game world***********************************************/
    level->addChar(lomy);
    /**set lomy start position*************************/
    dstrect.x = 400;
    dstrect.y = (768-lomy->getSize().y);///start position within screen bounds
    dstrect.w = 190;
    dstrect.h = 239;
    lomy->setPosition(dstrect);
    /**set lomy start position END*********************/

    level->addChar(goat);
    /**set goat monster start position*****************/
    dstrect.x = 600;
    dstrect.y = (788-goat->getSize().y);///start position within screen bounds(fix this so 768 will put goat on ground)
    dstrect.w = 213;
    dstrect.h = 239;
    goat->setPosition(dstrect);
    /**set goat monster start position END*************/
    /**add all characters into game world END*******************************************/



    /**SET UP COLLISION LOCATIONS FOR WORLD*************************************/
    
    /**data for all the collision points in the level******/
    int x[67] = {0,    0,    0,  0,  0,  0,0,0,0,0,0,0,167,363,423,591,711,795,843,875,944,983,1003,1007,1219,1351,1375,1515,1527,1559,1711,1887,1955,1967,2003,2158,2190,2198,2330,2378,2382,2390,2554,2618,2626,2662,2698,2854,2882,2978,2982,2994,3062,3074,3134,3138,3270,3278,3526,3582,3626,3626,3634,3878,3978,4022,4050};
    int y[67] = {968, 0,    622,808,968,1216,1520,1876,2224,2812,2948,2688,1792,532,2184,592,1848,2708,1604,0,2740,1364,1516,508,2288,960,2324,824,376,372,1744,1024,336,1576,408,2576,2708,1536,752,2332,2708,668,1072,1536,2008,2628,2364,956,1672,2112,704,2484,1436,1732,2048,868,1092,1496,2332,456,1824,2332,2640,856,2080,1228};
    int w[67]= {1024, 749,  121,175,308,2882,560,92,380,224,4186,217,283,312,631,352,1848,175,223,35,193,763,90,500,744,223,67,272,160,2475,133,120,519,49,452,216,217,355,48,289,289,208,295,85,121,139,84,270,127,172,355,192,79,121,433,163,583,664,61,259,343,169,310,208,166,136};
    int h[67] = {50,   144,  45, 33, 249,60,36,108,252,196,52,282,30,56,60,304,72,270,54,319,72,180,132,52,36,30,390,416,192,40,114,204,60,276,356,390,60,42,268,42,270,52,156,1468,66,84,54,500,84,76,90,508,750,96,72,108,54,48,246,270,66,96,364,60,90,42};
    /**data for all the collision points in the level END**/
    
    CollisionObject* obj[66];// = new CollisionObject();///list of collision locations
//    CollisionObject* obj1 = new CollisionObject();
//    CollisionObject* obj2 = new CollisionObject();
//    CollisionObject* obj3 = new CollisionObject();
    SDL_Rect collision;

    for (int i = 0; i < 1; i++)///apply all collision areas in the game
    {
        obj[i] = new CollisionObject();
        collision.x = x[i];
        collision.y = y[i];
        collision.w = w[i];
        collision.h = h[i];
        obj[i]->setPosition(collision);
        obj[i]->setCollidable();
        //level->addCollisionArea(obj[i]);
    }
    /**SET UP COLLISION LOCATIONS FOR WORLD END*********************************/

/***********************************GAME WORLD END********************************************/



/*********sound*********/
    music = Mix_LoadMUS( "../Golem Dragon Sword/TGDS Music fix.wav" );
//    scratch = Mix_LoadWAV( "scratch.wav" );
//    high = Mix_LoadWAV( "high.wav" );
//    med = Mix_LoadWAV( "medium.wav" );
//    low = Mix_LoadWAV( "low.wav" );

    //level->addCollisionArea(obj3);

    // centre the bitmap on screen

    //Mix_PlayMusic( music, -1 );
/*********sound END*****/
    
    /***********MENU SCREEN****************/
    SDL_Surface* menu = IMG_Load("../images/Intro1.png");
    if (!menu)
    {
        cout << "Unable to load png: " << SDL_GetError() << endl;
    }

    clip.x = 0;
    clip.y = 0;
    clip.w = 1024;
    clip.h = 768;
    //sheets.push_back(SDL_DisplayFormatAlpha(tmp));

    SDL_BlitSurface(menu, &clip, screen, NULL);
    menu = IMG_Load("../images/Intro2.png");
    SDL_Flip(screen);

    ///SDL_Delay(7000);

    SDL_BlitSurface(menu, &clip, screen, NULL);
    menu = IMG_Load("../images/Intro3.png");

    SDL_Flip(screen);


    ///SDL_Delay(1000);


        SDL_BlitSurface(menu, &clip, screen, NULL);

    menu = IMG_Load("../images/Intro4.png");

    SDL_Flip(screen);


    ///SDL_Delay(2000);

    SDL_BlitSurface(menu, &clip, screen, NULL);

    SDL_Flip(screen);

           // SDL_Event event;


    SDL_Delay(70);
    /***********MENU SCREEN END************/


///game loop
    // program main loop
    bool done = false;
    timer = SDL_GetTicks();
    lomy->setState(false);///why do i need this?
    while (!done)
    {

        delta = SDL_GetTicks() - timer;
        if (delta >= 100)
        {
            timer = SDL_GetTicks();

            // message processing loop
            SDL_Event event;

            while (SDL_PollEvent(&event))///events in here will only happen once per click
            {
                // check for messages
                switch (event.type)
                {
                    // exit if the window is closed
                    case SDL_QUIT:
                        done = true;
                    break;
                    // check for keypresses
                    case SDL_KEYDOWN:
                    // exit if ESCAPE is pressed
                        switch (event.key.keysym.sym)
                        {
                            case SDLK_ESCAPE:
                                done = true;
                            break;


                        } // end switch

                }
            }
            //cout << "tet" << endl;
            Uint8 *keystates = SDL_GetKeyState( NULL );
            if (keystates[SDLK_a])
            {
                lomy->setState(HIT);
            }
            else if (keystates[SDLK_RIGHT] )
            {
                lomy->setState(JUMP, true);
                lomy->setState(IDLE, true);
                lomy->setDirection(FORWARD);
                lomy->setState(RUN);
            }
            else if (keystates[SDLK_LEFT])
            {
                lomy->setState(JUMP, true);
                lomy->setState(IDLE, true);
                lomy->setDirection(BACKWARDS);
                lomy->setState(RUN);
            }
            else if (keystates[SDLK_DOWN])
            {
                lomy->setState(JUMP, true);
                lomy->setState(IDLE, true);
                lomy->setDirection(BACKWARDS);
                lomy->setState(RUN);
            }
            else
            {
                lomy->setState(IDLE);
            }


            //if(keystates[SDLK_SPACE])
            //   lomy->setState(JUMP);
            // end of message processing

            // DRAWING STARTS HERE
            //lomy->move(1000/delta);
            level->update(delta, screen);
            //cout  << "restart timer" << endl;
            // clear screen
         // end main loop
        }
        }
        // free loaded bitmap
        lomy->~Character();
        level->~World();
        for (int i = 0; i < 1; i++)
        {
            obj[i]->~CollisionObject();
        }

        // all is well ;)
        delete(music);
        printf("Exited cleanly\n");
        return 0;

}
