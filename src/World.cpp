#include "../include/World.h"
#ifdef __cplusplus
#include <cstdlib>
#else
#include <stdlib.h>
#endif
#ifdef __APPLE__
#include <SDL/SDL.h>
#else
#include <SDL/SDL.h>
#endif
#ifdef __linux__
#include <sys/time.h>
timeval t;
#define TIME gettimeofday (&t, NULL)
#else ifdef _WIN32
#include <windows.h>
#define TIME GetTickCount
#endif
#include <time.h>

World::World()
{
    //ctor
}

World::World(char* filename, char* charName, int frames, SDL_Rect frame): CollisionObject(filename, charName)
{
    //ctor


    spriteFrame = frame;


    gravity = -10.0;
    mass=10.0;

}

void World::addChar(Character* charr)
{
    string tmpName = charr->getName();
    characters[tmpName] = charr;
    characters[tmpName]->setCollidable();
}

void World::removeChar(string charr)
{
    charactersItr = characters.find(charr);
    charactersItr->second->~Character();
    characters.erase(charactersItr);
    removeCollisionArea(charr);
}

void World::apply_surface( int x, int y, SDL_Surface* source, SDL_Surface* destination, SDL_Rect* clip = NULL )
{
    //Holds offsets
    SDL_Rect offset;

    //Get offsets
    offset.x = x;
    offset.y = y;

    //Blit
    SDL_BlitSurface( source, clip, destination, &offset );
}

void World::addCollisionArea(CollisionObject* area)       ///add one collision area
{
    string tmpName = area->getName();
    collisionAreas[tmpName] = area;
    collisionAreas[tmpName]->setCollidable();

}

void World::removeCollisionArea(string area)
{
    collisionItr = collisionAreas.find(area);
    cout << "found " << charactersItr->first << endl;
    ///charactersItr->second->~CollisionObject();   is pointer really being taken care of??
    cout << "removed collision object" << endl;
    collisionAreas.erase(collisionItr);
}

char World::checkForCollisions()
{
    float top, bottom, left, right;
    float leftC, rightC, topC, bottomC;
    int colCount;
    //characters["lomy"]->botCol = false;
    characters["lomy"]->setCollidable();///DEFF NEED THIS
    for(collisionItr = collisionAreas.begin(); collisionItr != collisionAreas.end(); collisionItr++)
    {
        ///cout << "CHECKING COLLISIONS" << endl;
        collisionItr->second->hasCollision = false;
        collisionItr->second->setCollidable();
        ///reset collision info
        collisionItr->second->botCol = false;
        collisionItr->second->topCol = false;
        collisionItr->second->leftCol = false;
        collisionItr->second->rightCol = false;
        colCount = 0;
        ///cout << "CHECKING COLLISIONS 2" << endl;
        ///check potential collision areas

        collisionItr->second->botCol = collisionItr->second->collisionHorz(characters["lomy"]->top);///12
        collisionItr->second->topCol = collisionItr->second->collisionHorz(characters["lomy"]->bottom);///12
        ///cout << "GOAT IS " << collisionItr->second->left << endl;
        ///cout << "LOMY IS " << characters["lomy"]->right << endl;
        collisionItr->second->rightCol = collisionItr->second->collisionVert(characters["lomy"]->left);///ab COLLIDE WITH RIGHT SIDE OF OBJECT (lomy's left)
        collisionItr->second->leftCol = collisionItr->second->collisionVert(characters["lomy"]->right);///ab COLLIDE WITH LEFT SIDE OF OBJECT (lomy's right)
//
//        cout << "CHECKING COLLISIONS 3" << endl;
//

        ///left and right but not top or bottom = safe for left right
        ///top and bottom means only one way left or right
        ///top means you
        ///will always have 2
        //cout << "i = " << i << endl;



            if (collisionItr->second->rightCol || collisionItr->second->leftCol)///if between the two sides
            {
                ///cout << " AB check passed for element " << i << endl;
//                cout << "A = " << collisionItr->second->leftCol << endl;
//                cout << "B = " << collisionItr->second->rightCol << endl;
//                cout << "1 = " << collisionItr->second->topCol << endl;
//                cout << "2 = " << collisionItr->second->botCol << endl;


                if (collisionItr->second->botCol ||  collisionItr->second->topCol)///touching top or bottom
                {
                    ///cout << "on top/below " << i << "bot is " << collisionItr->second->topCol<< endl<< endl;
                    cout << "collide with " << collisionItr->first << endl;
                    collisionItr->second->hasCollision = true;
                return 's';
                }

                //characters["lomy"]->setPosition(SDL_Rect{characters["lomy"]->getPosition()->x, 0});

            }
            else if (collisionItr->second->topCol || collisionItr->second->botCol)///if between the top and bottom
            {
                ///cout << " 1-2 check passed for element " << i << endl;
//                cout << "A = " << collisionItr->second->leftCol << endl;
//                cout << "B = " << collisionItr->second->rightCol << endl;
//                cout << "1 = " << collisionItr->second->topCol << endl;
//                cout << "2 = " << collisionItr->second->botCol << endl;


                if (collisionItr->second->leftCol ||  collisionItr->second->rightCol)///if you are touching a side
                {
                    ///cout << "beside "  <<i << endl<< endl;
                    return 't';
                }

            }
            ///else
                ///cout << "no collisions" << endl;
            ///cout << "FINISHED CHECKING COLLISIONS" << endl << endl;
        //}
    }

return 'n';

}


void World::drawWorld(SDL_Surface* screen)
{
    Uint32 color;
    // color = SDL_MapRGB(screen->format, 255,0,0);


    SDL_BlitSurface(this->getImage(), this->getFrame(), screen, NULL);

    for(charactersItr = characters.begin(); charactersItr != characters.end(); charactersItr++)
    {
        SDL_BlitSurface(charactersItr->second->getImage(), charactersItr->second->getFrame(), screen, charactersItr->second->getPosition());
    }


    for(collisionItr = collisionAreas.begin(); collisionItr != collisionAreas.end(); collisionItr++)
    {
        //SDL_FillRect(this->getImage(), collisionItr->second->getPosition(), color);///add colours so you can tell which wall you are debugging.  then make char not stick in tall left side of wall
    }

    SDL_Flip(screen);
}

void World::movement(int delta)
{
    ///delta is in seconds
    float fdelta = delta;
    fdelta /= 6;

    if (characters["lomy"]->force.x > 0.0)                                                      ///if you have a positive force
    {
        characters["lomy"]->force.x *= 0.3;                                                     ///apply new force(tweak this)
        characters["lomy"]->accel.x = (characters["lomy"]->force.x)/characters["lomy"]->mass;   ///calculate new acceleration
        if (characters["lomy"]->velocity.x < characters["lomy"]->maxVel.x)                      ///if you're not at max speed
            characters["lomy"]->velocity.x += (characters["lomy"]->accel.x)*delta*delta;        ///speed up!!
    }
    /************************************************************/
    ///plus symbol means down, minus means up

    //if (states[JUMP] == false)
    //    force.y = gravity*mass;
    if (characters["lomy"]->states[JUMP])           ///if lomy is jumping
    {
        characters["lomy"]->force.y -= 100.0;       ///apply force
        characters["lomy"]->states[JUMP] == false;  ///turn off jump state to show that you've done the action already
    }

    ///apply downword force(gravity)
    characters["lomy"]->force.y += (characters["lomy"]->gravity/2)*(fdelta)*(fdelta);       ///apply new force(tweak this)
    characters["lomy"]->accel.y += characters["lomy"]->force.y/characters["lomy"]->mass;    ///calculate new acceleration
    //velocity.y += accel.y;
    characters["lomy"]->velocity.y += (characters["lomy"]->accel.y)*(fdelta);               ///update velocity

    //cout << "moovin!!!";
    //cout << velocity.y << endl;
    ///if on the ground (??)
    if (botCol)
        characters["lomy"]->setState(JUMP, true);///jump

    if (!botCol || characters["lomy"]->states[JUMP])///this is disabled
    {
        //characters["lomy"]->spriteLocation.y+=characters["lomy"]->velocity.y*(fdelta);
        //cout << "yo" << endl;
    }

    char c = checkForCollisions();///run collision detection

    /**move all charactersy******************************/
    for(charactersItr = characters.begin(); charactersItr != characters.end(); charactersItr++)
    {
        if (charactersItr->first != "lomy")
        {
            charactersItr->second->setState(RUN);
            TIME;
            srand ( t.tv_usec );
            //cout << "moving " << charactersItr->first <<endl;
            /** generate secret number: */
            int numba = rand() % 8;

            //charactersItr->second->setDirection(FORWARD);
            charactersItr->second->move(-10*charactersItr->second->getDirection(), 0);
            if (numba == 0 || spriteFrame.x + charactersItr->second->getPosition()->x > 3700 || spriteFrame.x + charactersItr->second->getPosition()->x < 300)
            {
                charactersItr->second->setDirection(charactersItr->second->getDirection()*-1);
            }
        }
        else
        {
            charactersItr->second->move(int (charactersItr->second->velocity.x*charactersItr->second->direction), 0);
            if (spriteFrame.x + charactersItr->second->getPosition()->x > 3700)///checking if camera has moved too far
            {
                charactersItr->second->move(int((charactersItr->second->velocity.x*charactersItr->second->direction)*-1), 0);
                spriteFrame.x = 3100;
            }
            else if ( spriteFrame.x + charactersItr->second->getPosition()->x < 300)
            {
                spriteFrame.x = 0;
            }
            else
            {
                /******end move characters********************/
                if ( charactersItr->second->getPosition()->x > 600 )///bounds check for right side of screen
                {
                    spriteFrame.x += charactersItr->second->velocity.x*charactersItr->second->direction;
                    charactersItr->second->move(int((charactersItr->second->velocity.x*charactersItr->second->direction)*-1), 0);
                }
                else if (characters["lomy"]->getPosition()->x < 300 )///bounds check for left side of screen
                {
                    spriteFrame.x -= charactersItr->second->velocity.x*charactersItr->second->direction*-1;
                    charactersItr->second->move(int((charactersItr->second->velocity.x*charactersItr->second->direction)*-1), 0);
                }
                if ( characters["lomy"]->getPosition()->y > 768-239 )///bounds check for top of screen
                {
                    //characters["lomy"]->move(0, int((characters["lomy"]->velocity.y*characters["lomy"]->direction)*-1));
                    //characters["lomy"]->accel.x = 0;
                    //spriteFrame.x -= (characters["lomy"]->velocity.x*characters["lomy"]->direction) - 95/2.0;///move character back to where it was(check bedmas)
                }
            }
        }

        if (c == 's')   ///if there is a side collision
        {
            if (charactersItr->first == "lomy") ///if lomy, move lomy back
                charactersItr->second->move(int((charactersItr->second->velocity.x*charactersItr->second->direction)*-1), 0);
            else                                ///if not lomy
            {
                charactersItr->second->move(int((-5*charactersItr->second->direction)*-1), 0);  ///move monster back
                if (characters["lomy"]->getSwinging() && charactersItr->second->hasCollision)                                         ///if the keytar is being swung during a collision
                {
                    cout << "HIT!!!!!!!!!!!!!!!" << endl<<  "HIT!!!!!!!!!!!!!!!" << endl<< "HIT!!!!!!!!!!!!!!!" << endl<< "HIT!!!!!!!!!!!!!!!" << endl<< "HIT!!!!!!!!!!!!!!!" << endl<< "HIT!!!!!!!!!!!!!!!" << endl;
                    removeChar(charactersItr->first);
                }
            }
        }

        else if  (c == 't') ///if there is a top collisiom
        {
            //cout << "top" << endl;
        }


    }

}

void World::update(int timer, SDL_Surface* screen)
{
        //cout << "position x: " << characters["lomy"]->spriteLocation.x << endl;///lomy's position
    movement(1000/timer);
    SDL_FillRect(screen, &screen->clip_rect, SDL_MapRGB(screen->format, 0, 255, 0));
    drawWorld(screen);
}

World::~World()
{
    //dtor
    SDL_FreeSurface(sheets.back());
}
