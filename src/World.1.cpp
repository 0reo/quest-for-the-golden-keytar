#include "../include/World.h"
#ifdef __cplusplus
#include <cstdlib>
#else
#include <stdlib.h>
#endif
#ifdef __APPLE__
#include <SDL/SDL.h>
#else
#include <SDL/SDL.h>
#endif
#include <time.h>

World::World()
{
    //ctor
}

World::World(char* filename, char* charName, int frames, SDL_Rect frame): CollisionObject(filename, charName)
{
    //ctor


    spriteFrame = frame;


    gravity = -10.0;
    mass=10.0;

}

void World::addChar(Character* charr)
{
    string tmpName = charr->getName();
    characters[tmpName] = charr;
    characters[tmpName]->setCollidable();
}

void World::apply_surface( int x, int y, SDL_Surface* source, SDL_Surface* destination, SDL_Rect* clip = NULL )
{
    //Holds offsets
    SDL_Rect offset;

    //Get offsets
    offset.x = x;
    offset.y = y;

    //Blit
    SDL_BlitSurface( source, clip, destination, &offset );
}

void World::addCollisionArea(CollisionObject* area)       ///add one collision area
{
    collisionAreas.push_back(area);
}

char World::checkForCollisions()
{
    float top, bottom, left, right;
    float leftC, rightC, topC, bottomC;
    int colCount;
    //characters["lomy"]->botCol = false;
    characters["lomy"]->setCollidable();///DEFF NEED THIS
    for ( int i = 0 ; i < collisionAreas.size(); i++ )
    {
        ///cout << "CHECKING COLLISIONS" << endl;
    collisionAreas[i]->setCollidable();
        ///reset collision info
        collisionAreas[i]->botCol = false;
        collisionAreas[i]->topCol = false;
        collisionAreas[i]->leftCol = false;
        collisionAreas[i]->rightCol = false;
        colCount = 0;
        ///cout << "CHECKING COLLISIONS 2" << endl;
        ///check potential collision areas

        collisionAreas[i]->botCol = collisionAreas[i]->collisionHorz(characters["lomy"]->top);///12
        collisionAreas[i]->topCol = collisionAreas[i]->collisionHorz(characters["lomy"]->bottom);///12
        ///cout << "GOAT IS " << collisionAreas[i]->left << endl;
        ///cout << "LOMY IS " << characters["lomy"]->right << endl;
        collisionAreas[i]->rightCol = collisionAreas[i]->collisionVert(characters["lomy"]->left);///ab COLLIDE WITH RIGHT SIDE OF OBJECT (lomy's left)
        collisionAreas[i]->leftCol = collisionAreas[i]->collisionVert(characters["lomy"]->right);///ab COLLIDE WITH LEFT SIDE OF OBJECT (lomy's right)
//
//        cout << "CHECKING COLLISIONS 3" << endl;
//

        ///left and right but not top or bottom = safe for left right
        ///top and bottom means only one way left or right
        ///top means you
        ///will always have 2
        //cout << "i = " << i << endl;



            if (collisionAreas[i]->rightCol || collisionAreas[i]->leftCol)///if between the two sides
            {
                ///cout << " AB check passed for element " << i << endl;
//                cout << "A = " << collisionAreas[i]->leftCol << endl;
//                cout << "B = " << collisionAreas[i]->rightCol << endl;
//                cout << "1 = " << collisionAreas[i]->topCol << endl;
//                cout << "2 = " << collisionAreas[i]->botCol << endl;


                if (collisionAreas[i]->botCol ||  collisionAreas[i]->topCol)///touching top or bottom
                {
                    ///cout << "on top/below " << i << "bot is " << collisionAreas[i]->topCol<< endl<< endl;
                return 's';
                }

                //characters["lomy"]->setPosition(SDL_Rect{characters["lomy"]->getPosition()->x, 0});

            }
            else if (collisionAreas[i]->topCol || collisionAreas[i]->botCol)///if between the top and bottom
            {
                ///cout << " 1-2 check passed for element " << i << endl;
//                cout << "A = " << collisionAreas[i]->leftCol << endl;
//                cout << "B = " << collisionAreas[i]->rightCol << endl;
//                cout << "1 = " << collisionAreas[i]->topCol << endl;
//                cout << "2 = " << collisionAreas[i]->botCol << endl;


                if (collisionAreas[i]->leftCol ||  collisionAreas[i]->rightCol)///if you are touching a side
                {
                    ///cout << "beside "  <<i << endl<< endl;
                    return 't';
                }

            }
            ///else
                ///cout << "no collisions" << endl;
            ///cout << "FINISHED CHECKING COLLISIONS" << endl << endl;
        //}
    }

return 'n';

}


void World::drawWorld(SDL_Surface* screen)
{
    Uint32 color;
    // color = SDL_MapRGB(screen->format, 255,0,0);


    SDL_BlitSurface(this->getImage(), this->getFrame(), screen, NULL);

    for(charactersItr = characters.begin(); charactersItr != characters.end(); charactersItr++)
    {
        SDL_BlitSurface(charactersItr->second->getImage(), charactersItr->second->getFrame(), screen, charactersItr->second->getPosition());
    }


    for ( int i = 0 ; i < collisionAreas.size(); i++ )
    {
        SDL_FillRect(this->getImage(), collisionAreas[i]->getPosition(), color);///add colours so you can tell which wall you are debugging.  then make char not stick in tall left side of wall
    }

    SDL_Flip(screen);
}

void World::movement(int delta)
{
    ///delta is in seconds
    float fdelta = delta;
    fdelta /= 6;

    if (characters["lomy"]->force.x > 0.0)                                                      ///if you have a positive force
    {
        characters["lomy"]->force.x *= 0.3;                                                     ///apply new force(tweak this)
        characters["lomy"]->accel.x = (characters["lomy"]->force.x)/characters["lomy"]->mass;   ///calculate new acceleration
        if (characters["lomy"]->velocity.x < characters["lomy"]->maxVel.x)                      ///if you're not at max speed
            characters["lomy"]->velocity.x += (characters["lomy"]->accel.x)*delta*delta;        ///speed up!!
    }
    /************************************************************/
    ///plus symbol means down, minus means up

    //if (states[JUMP] == false)
    //    force.y = gravity*mass;
    if (characters["lomy"]->states[JUMP])           ///if lomy is jumping
    {
        characters["lomy"]->force.y -= 100.0;       ///apply force
        characters["lomy"]->states[JUMP] == false;  ///turn off jump state to show that you've done the action already
    }

    ///apply downword force(gravity)
    characters["lomy"]->force.y += (characters["lomy"]->gravity/2)*(fdelta)*(fdelta);       ///apply new force(tweak this)
    characters["lomy"]->accel.y += characters["lomy"]->force.y/characters["lomy"]->mass;    ///calculate new acceleration
    //velocity.y += accel.y;
    characters["lomy"]->velocity.y += (characters["lomy"]->accel.y)*(fdelta);               ///update velocity

    //cout << "moovin!!!";
    //cout << velocity.y << endl;
    ///if on the ground (??)
    if (botCol)
        characters["lomy"]->setState(JUMP, true);///jump

    if (!botCol || characters["lomy"]->states[JUMP])///this is disabled
    {
        //characters["lomy"]->spriteLocation.y+=characters["lomy"]->velocity.y*(fdelta);
        //cout << "yo" << endl;
    }


    //characters["lomy"]->spriteLocation.x += characters["lomy"]->velocity.x*characters["lomy"]->direction/2.0;
    characters["lomy"]->move(int (characters["lomy"]->velocity.x*characters["lomy"]->direction/2.0), 0);
    ///spriteFrame.x += characters["lomy"]->velocity.x*characters["lomy"]->direction/2.0;  uncomment this, it makes the screen move

    //spriteFrame.y -= characters["lomy"]->velocity.x*characters["lomy"]->direction;
    //spriteFrame.x = 15;
    char c = checkForCollisions();///run collision detection
    //if (spriteFrame.y-characters["lomy"]->spriteLocation.y < 0 || spriteFrame.y-characters["lomy"]->spriteLocation.y < 3000)
        //  spriteFrame.y += characters["lomy"]->velocity.y*characters["lomy"]->direction;
    //cout << "collision check = ";
    cout << this->getPosition()->x << endl;
    ///-3162
//                cout << "A = " << characters["lomy"]->top << endl;
//                cout << "B = " << characters["lomy"]->bottom << endl;
//                cout << "1 = " << characters["lomy"]->left << endl;
//                cout << "2 = " << characters["lomy"]->right << endl;

        if ( characters["lomy"]->getPosition()->x > 800 )///bounds check for right side of screen
        {
            characters["lomy"]->move(int((characters["lomy"]->velocity.x*characters["lomy"]->direction)*-1), 0);
            //characters["lomy"]->accel.x = 0;
            //spriteFrame.x -= (characters["lomy"]->velocity.x*characters["lomy"]->direction) - 95/2.0;///move character back to where it was(check bedmas)
        }
        if (characters["lomy"]->getPosition()->x < 0 )///bounds check for left side of screen
        {
            characters["lomy"]->move(int((characters["lomy"]->velocity.x*characters["lomy"]->direction)*-1), 0);
            //characters["lomy"]->accel.x = 0;
            //spriteFrame.x += (characters["lomy"]->velocity.x*characters["lomy"]->direction);///move character back to where it was(check bedmas)
        }
        if ( characters["lomy"]->getPosition()->y > 768-239 )///bounds check for top of screen
        {
            characters["lomy"]->move(0, int((characters["lomy"]->velocity.y*characters["lomy"]->direction)*-1));
            //characters["lomy"]->accel.x = 0;
            //spriteFrame.x -= (characters["lomy"]->velocity.x*characters["lomy"]->direction) - 95/2.0;///move character back to where it was(check bedmas)
        }

        ///if (this->getPosition()->x + characters["lomy"]->getPosition()->x+586 > 4186 || this->getPosition()->x + characters["lomy"]->getPosition()->x+220 < 400)///checking if camera has moved too far
            ///characters["lomy"]->move(int((characters["lomy"]->velocity.x*characters["lomy"]->direction)*-1), 0);
            //spriteFrame.x -= characters["lomy"]->velocity.x*characters["lomy"]->direction;///move character back to where it was(check bedmas)




            //characters["lomy"]->spriteLocation.y-=7;
        //}
    //else if ( c == 'n' && spriteFrame.y < 2000 )
    //{
       // characters["lomy"]->spriteLocation.y+=7;
        //spriteFrame.y +=7;
    //}
        //cout << spriteFrame.y << " frames" << endl;
    //else if ( checkForCollisions() == 'r')
        //characters["lomy"]->spriteLocation.y+=characters["lomy"]->velocity.y*(fdelta);


    //cout << "the number = " << spriteFrame.x+characters["lomy"]->spriteLocation.x << endl;
    //cin.get();

    /**move goat******************************/
    characters["goat"]->setState(RUN);
    srand ( time(NULL) );

    /** generate secret number: */
    int numba = rand() % 500 +1;

    characters["goat"]->setDirection(FORWARD);
    characters["goat"]->move(-5, 0);
    if (numba % 2)
    {
        characters["goat"]->setDirection(BACKWARDS);
        characters["goat"]->move(10, 0);

    }
    /******end move goat********************/


        if (c == 's')
        {
            cout << "side" << endl;
            characters["lomy"]->move(int((characters["lomy"]->velocity.x*characters["lomy"]->direction)*-1), 0);
            characters["goat"]->move(int((-5*characters["goat"]->direction)*-1), 0);
        }

        else if  (c == 't')
        {
            cout << "top" << endl;
        }

}

void World::update(int timer, SDL_Surface* screen)
{
        //cout << "position x: " << characters["lomy"]->spriteLocation.x << endl;///lomy's position
    movement(1000/timer);
    SDL_FillRect(screen, &screen->clip_rect, SDL_MapRGB(screen->format, 0, 255, 0));
    drawWorld(screen);
}

World::~World()
{
    //dtor
    SDL_FreeSurface(sheets.back());
}
