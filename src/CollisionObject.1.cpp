#include "../include/CollisionObject.h"

CollisionObject::CollisionObject()
{
    //ctor
name = "no name";
}

CollisionObject::CollisionObject(char* filename, char* charName)
{
    //ctor
    name = charName;
    tmp = IMG_Load(filename);
    if (!tmp)
    {
        cout << "Unable to load png: " << SDL_GetError() << endl;
    }

    sheets.push_back(SDL_DisplayFormatAlpha(tmp));
    SDL_FreeSurface(tmp);
    topCol = false;
    botCol = false;
    leftCol = false;
    rightCol=false;
    FLIP_VERTICAL = 1;
    FLIP_HORIZONTAL = 2;
    currentSheet = 0;

}

CollisionObject::~CollisionObject()
{
    //dtor
}
SDL_Surface* CollisionObject::getImage()
{
    return sheets[currentSheet];
}

SDL_Rect* CollisionObject::getFrame()
{
    return &spriteFrame;
}

void CollisionObject::setPosition(SDL_Rect position)
{
    spriteLocation.x = position.x;
    spriteLocation.y = position.y;
}

SDL_Rect* CollisionObject::getPosition()
{
    return &spriteLocation;
}

void CollisionObject::move(int x, int y)
{
    SDL_Rect tmp;
    tmp.x = getPosition()->x + x;
    tmp.y = getPosition()->y + y;

    setPosition(tmp);
}


void CollisionObject::setCollidable(SDL_Rect colSpace)
{
    if (colSpace.w ==0 && colSpace.x == 0)
        colSpace = *getPosition();
    right =  colSpace.x + colSpace.w;///rightmost pixel
    left = colSpace.x;///left pixel
    top = colSpace.y;///top pixel
    bottom = colSpace.y + colSpace.h;///bottom-pixel
///    cout << name << " is collidable" << endl;

}

bool CollisionObject::collisionHorz(float side)///checks 1 and two (under top, below bottpm)
{
    //cout << name << " is checking for collisions horz" << endl;

    if(side >= top && side <= bottom)
    {

        return true;
    }
    return false;
}

bool CollisionObject::collisionVert(float side)///this works
{
    //cout << name << " is checking for collisions vert" << endl;

    if(side <= right && side >= left)
    {
        return true;
    }
    return false;
}


void CollisionObject::applyForce(Vector2D frc)
{
    force.x += frc.x;
    force.y += frc.y;
}


Vector2D CollisionObject::getForce()
{
    return force;
}

Uint32 CollisionObject::get_pixel32( SDL_Surface *surface, int x, int y )
{
    //Convert the pixels to 32 bit
    Uint32 *pixels = (Uint32 *)surface->pixels;

    //Get the requested pixel
    return pixels[ ( y * surface->w ) + x ];
}

void CollisionObject::put_pixel32( SDL_Surface *surface, int x, int y, Uint32 pixel )
{
    //Convert the pixels to 32 bit
    Uint32 *pixels = (Uint32 *)surface->pixels;

    //Set the pixel
    pixels[ ( y * surface->w ) + x ] = pixel;
}

SDL_Surface* CollisionObject::flip_surface( SDL_Surface *surface=NULL, int flags = NULL )
{
    //Pointer to the soon to be flipped surface

}
