#include "../include/CollisionObject.h"

CollisionObject::CollisionObject()
{
    //ctor

}

CollisionObject::CollisionObject(char* filename, char* charName)
{
    //ctor
    name = charName;
    tmp = IMG_Load(filename);
    if (!tmp)
    {
        cout << "Unable to load png: " << SDL_GetError() << endl;
    }

    sheets.push_back(SDL_DisplayFormatAlpha(tmp));
    SDL_FreeSurface(tmp);
    topCol = false;
    botCol = false;
    leftCol = false;
    rightCol=false;
    FLIP_VERTICAL = 1;
    FLIP_HORIZONTAL = 2;

}

CollisionObject::~CollisionObject()
{
    //dtor
}
SDL_Surface* CollisionObject::getImage()
{
    return sheets[currentSheet];
}

SDL_Rect* CollisionObject::getFrame()
{
    return &spriteFrame;
}

void CollisionObject::setPosition(SDL_Rect position)
{
    spriteLocation = position;
}

SDL_Rect* CollisionObject::getPosition()
{
    return &spriteLocation;
}

void CollisionObject::setCollidable()
{

    right =  getPosition()->x + getPosition()->w;///rightmost pixel
    left = getPosition()->x;///left pixel
    top = getPosition()->y;///top pixel
    bottom = getPosition()->y + getPosition()->h;///bottom-pixel
}

bool CollisionObject::collisionHorz(float side)///checks 1 and two (under top, below bottpm)
{
    if(side >= top && side <= bottom)
    {
        return true;
    }
    return false;
}

bool CollisionObject::collisionVert(float side)
{
    if(side <= right && side >= left)
    {
        return true;
    }
    return false;
}


void CollisionObject::applyForce(Vector2D frc)
{
    force.x += frc.x;
    force.y += frc.y;
}


Vector2D CollisionObject::getForce()
{
    return force;
}

Uint32 CollisionObject::get_pixel32( SDL_Surface *surface, int x, int y )
{
    //Convert the pixels to 32 bit
    Uint32 *pixels = (Uint32 *)surface->pixels;

    //Get the requested pixel
    return pixels[ ( y * surface->w ) + x ];
}

void CollisionObject::put_pixel32( SDL_Surface *surface, int x, int y, Uint32 pixel )
{
    //Convert the pixels to 32 bit
    Uint32 *pixels = (Uint32 *)surface->pixels;

    //Set the pixel
    pixels[ ( y * surface->w ) + x ] = pixel;
}

SDL_Surface* CollisionObject::flip_surface( SDL_Surface *surface=NULL, int flags = NULL )
{
    //Pointer to the soon to be flipped surface

}
