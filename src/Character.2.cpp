#include "../include/Character.h"
#ifdef __cplusplus
#include <cstdlib>
#else
#include <stdlib.h>
#endif
#ifdef __APPLE__
#include <SDL/SDL.h>
#else
#include <SDL/SDL.h>
#endif
Character::Character()
{
    //ctor
}

Character::Character(char* filename, char* charName, int frames, SDL_Rect frame): CollisionObject(filename, charName)
{
    //ctor
///divide by 133 to get meters
///mult by 1.7 to get units
///



    spriteFrame = frame;
    direction = FORWARD;

    maxVel= {20.0, 20.0};
    velocity= {0.0, 0.0};
    accel= {0.0, 0.0};
    gravity = 17;/// m/s^2 converted to units /s^s
    mass=20.0; ///mass converted to units from kg
    jforce=25.0;


    states[RUN]=false;
    states[JUMP]=false;
    states[COLLIDE]=false;
    frameSize = {spriteFrame.w - spriteFrame.x, spriteFrame.h - spriteFrame.y};
    onGround = true;
    directiony = 1;
    force.x = 0.0;
    force.y = gravity*mass;


    health=5;
    strength = 5;
}

char* Character::getName()
{
    return name;
}

Vector2D Character::getSize()
{
    return frameSize;
}

void Character::addsheet(char* filename, int frames)
{
    tmp = IMG_Load(filename);
    if (!tmp)
    {
        cout << "Unable to load png: " << SDL_GetError() << endl;
    }

    sheets.push_back(SDL_DisplayFormatAlpha(tmp));
    SDL_FreeSurface(tmp);

}

void Character::addAction(char* name, int frames, int position)///add info about how many frames an animation is and what frame it starts at.  make it so that you can edit actions
{
    Action tmp = {name, frames, position};
    actions.push_back(tmp);
}

void Character::setDirection(int i)
{
    //cout << "velocity = " << velocity.x << endl;
    //cout << "direction = " << direction << endl;


    switch (i)
    {
    case FORWARD:
        direction=FORWARD;
        currentSheet = 0;
        break;
    case BACKWARDS:
        direction=BACKWARDS;
        currentSheet = 1;
        break;
    }
}

void Character::jump(float jumpForce=0, int step=0)
{
    //cout << states[JUMP] << endl;

    if (states[JUMP])
    {
        if (velocity.y >= 0.0)
        {
            if (spriteFrame.x < frameSize.x*JUMP || spriteFrame.x > frameSize.x*11)
                spriteFrame.x = frameSize.x*JUMP;
            else if (spriteFrame.x < frameSize.x*11)
                spriteFrame.x += frameSize.x;
        }
        else
        {
            //if (spriteFrame.x != frameSize.x*12 && spriteFrame.x != frameSize.x*13)
            //  spriteFrame.x = frameSize.x*12;
            //else if (spriteFrame.x < frameSize.x*13)
            spriteFrame.x = frameSize.x*12;
        }
    }
}

void Character::run(float vel = 0.0)
{
    Action tmp;///temporary holder for selected action
    tmp = actions[0];///replace this with a search to look for Run action

    accel.x += 1.0;
    states[IDLE] = false;
    if (states[JUMP] == false)
    {
        spriteFrame.x += frameSize.x;
        if (spriteFrame.x >= frameSize.x*tmp.frames) ///if sprite frame position is more than the size of the sprite sheet
            spriteFrame.x = 0;              ///reset sprite frame position
    }

    states[RUN] = false;
}

void Character::hit()
{
    if (spriteFrame.x > frameSize.x*24)
        spriteFrame.x = HIT;
    spriteFrame.x += frameSize.x;
    states[HIT] = false;
}

void Character::idle()
{
    velocity = {0.0, 0.0};
    accel.x = 0.0;
    accel.y = 0.0;
    if (states[JUMP] == false)
    {
        //if (spriteFrame.x <= frameSize.x*17)
         //   spriteFrame.x = frameSize.x*(18);
        //else if (spriteFrame.x >= frameSize.x*(18))
            spriteFrame.x = frameSize.x*17;
    }
    states[IDLE] = false;
}
void Character::setState(int state, bool off)
{
    int i;

    for(statesItr = states.begin(); statesItr != states.end(); statesItr++)
    {
        if (state == statesItr->first && off)
            statesItr->second = false;  ///turn off; set to false
        else if (state == statesItr->first)
            statesItr->second = true;   ///turn on; set to true
    }
//        if (botCol)
//        {
//            velocity.y=0.0;
//            accel.y = 0.0;
//            force.y * -1.0;
//            //spriteLocation.y -= 1;
//            cout << "col" << endl;
//        }
//        else
//        {
    if (states[RUN])
    {
        //accel.x = 1.5;
        force.x += 5.0;
//            if (state == RUN && off)
//            {
//                states[RUN] = false;
//                velocity.x = 0.0;
//                accel.x = 0.0;
//            }
        run();
    }
    if (states[CROUCH])/*******************************/
    {
        cout << "courcjh" << endl;
        states[IDLE] = false;
        if (states[JUMP] == false)
        {
        accel.x += 0.0;
        if (spriteFrame.x >= frameSize.x*IDLE)
             spriteFrame.x = 0;
        spriteFrame.x += frameSize.x;
        states[RUN] = false;
    }
    }
    if (states[JUMP] == true)
    {
        //if (botCol)
        jump();
    }

    if (states[HIT])
    {
        hit();
    }
    if (states[TAKEHIT])
    {

    }
    if (states[IDLE])
    {
        idle();
    }
    if (states[DIE])
    {

    }
    if (states[TRANSFORM])
    {

    }
    if (states[STUN])
    {

    }

//        }

}
